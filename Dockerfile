FROM mhart/alpine-node
#Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app 
#Install App dependencies
COPY . /usr/src/app/
#RUN npm install
#Bundle app source
EXPOSE 3000 
CMD ["npm", "start"]
